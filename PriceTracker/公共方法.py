# + ：可以用来拼接字符串，元组，列表
print('hello' + 'world')
print(('good', 'yes') + ('hi', 'ok'))
print([1, 2, 3] + [4, 5, 6])

# - :只能用于集合求差集
print({1, 2, 3} - {2, 3})

# * :可以用于字符串元组和列表表示重复多次
print('hello' * 3)
print([1, 2, 3] * 3)
print((1, 2, 3) * 3)

# in : 成员运算符
print('a' in 'abc')
print(1 in [1, 2, 3])
print(4 in (6, 4, 5))
# in用于字典判断key是否存在
print('zhangsan' in {'name': 'zhangsan', 'age': 18, 'height': 180})
print('name' in {'name': 'zhangsan', 'age': 18, 'height': 180})
print(3 in {1, 2, 3})

# 代下标的遍历
# x = [12, 34, 54, 65, 26, 75]
x = (12, 34, 54, 65, 26, 75)

for i,v in enumerate(x):
    print(i, v)
