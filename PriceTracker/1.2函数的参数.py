# def  函数名(参数):
#   函数要执行的操作

def tell_story(place, person1, person2):  # place,person1,person2是形参
    print('从前有座山')
    print('山上有座' + place)
    print('庙里有个' + person1)
    print('还有一个' + person2)
    print(person1 + '在给' + person2 + '讲故事')


tell_story('房子', '智障', '傻子')  # '房子','智障','傻子'是实参
tell_story(place='厕所', person1='马', person2='牛')  # 变量赋值实参
