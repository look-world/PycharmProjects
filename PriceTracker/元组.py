# 元组和列表的区别是元组是不可变类型
words = ['heh', 'hi', 'hello', 'wa']  # 列表
nums = (1, 2, 3, 5, 9, 8, 8)  # 元组

print(nums[3])
# nums[3] = 10     #会报错
print(nums.index(3))  # 查找
print(nums.count(8))  # 计数
# 特殊情况
# ages = (18)    #这是一个int类型，不是元组
ages = (18,)
print(type(ages))
