# os 全称 OperationSystem操作系统
# os 模块里提供的方法就是用来调用操作系统的方法
import os

# os.name ==>获取操作系统的名字   Windows系列==>nt   非Windows ==> posix

print(os.name)  # nt
print(os.sep)  # 路径分隔符 Windows  \   非Windows  /

# abspath  ==> 获取绝对路径
print(os.path.abspath('Day1.py'))

# isdir  ==> 判断是否为文件夹
print(os.path.isdir('1.1函数的介绍.py'))  # False
print(os.path.isdir('.idea'))  # True

# isfile ==>  是否为文件
print(os.path.isfile('1.2函数的参数.py'))

# exists ==> 是否存在
print(os.path.exists('1.4函数的返回值.py'))

file_name = '2020.12.01.demo.py'
# print(file_name.rpartition('.'))    #rpartition()方法用'.'切割成为一个元组

print(os.path.splitext(file_name))

# os 里的其他方法介绍
# os.getcwd()  # 获取当前工作目录，即当前python脚本工作目录
# os.chdir('test')  # 改变当前脚本工作目录，相当于shell下的cd命令
# os.rename('毕业论文.txt', '毕业论文-最终版.txt')  # 文件重命名
# os.remove('毕业论文.txt')  # 删除文件
# os.rmdir('demo')  # 删除空文件夹
# os.removedirs('demo')  # 删除空文件夹
# os.mkdir('demo')  # 创建一个文件夹
# os.chdir('C:\\')  # 切换工作目录
# os.listdir('C:\\')  # 列出指定目录所有的文件和文件夹
# os.environ  # 获取到环境配置
# os.environ.get('PATH')  # 获取指定的环境配置
