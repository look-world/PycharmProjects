def te(x, y):
    a = x // y
    b = x % y
    # 一般情况下，一个函数只执行一个return语句
    # 特殊情况（finally语句）下一个函数可能会执行多个return语句

    # return [a, b]
    # return {'a': a, 'b': b}
    # return (a, b)
    return a, b  # 表示一个函数的结束


result = te(13, 5)  # 2,3
print('商是{},余数是{}'.format(result[0], result[1]))

shang, yushu = te(14, 6)
print('商是{},余数是{}'.format(shang, yushu))
