import time

start = time.time()  # time模块里的time（）方法，可以获取当前时间的时间戳
# 时间戳是从 1970-01-01 00:00:00  UTC 到现在的秒数
# 从1970-01-01 00:00:00  UTC~2020-11-30  7：44  UTC
x = 0
for i in range(1, 100000000):
    x += i

print(x)
end = time.time()
print('代码运行耗时{}秒'.format(end - start))

start = time.time()
print('hello')
time.sleep(3)
print('world')
end = time.time()
print('代码运行耗时{}秒'.format(end - start))
