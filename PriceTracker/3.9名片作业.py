students = []
while True:
    print('________________________')
    print('名片管理系统 V1.0')
    print('1:添加名片 \n2:删除名片\n3:修改名片\n4:查询名片\n5:显示所有名片\n6:退出系统')
    print('________________________')
    x = input('请输入要进行的操作（数字）:')
    if x == '1' or x == '2' or x == '3' or x == '4' or x == '5' or x == '6':
        if x == '1':
            name = input('请输入姓名:')
            for student in students:
                if student['name'] == name:
                    print('此用户名已被占用，请重新输入')
                    break
            else:
                tel = input('请输入手机号:')
                qq = input('请输入QQ号:')
                students.append({'name': name, 'tel': tel, 'qq': qq})
                print(students)
        elif x == '2':
            num1 = int(input('请输入要删除的序号:'))
            del students[num1]
            print('成功删除序号为{}的名片'.format(num1))
            print('删除后的名片册为{}'.format(students))
        elif x == '3':
            num2 = int(input('请输入要修改的序号:'))
            print('您要修改的信息是:姓名:{},电话:{},QQ:{}'.format(students[num2]['name'], students[num2]['tel'],
                                                      students[num2]['qq']))
            new_name = input('请输入新姓名:')
            students[num2]['name'] = new_name
            new_tel = input('请输入新手机号:')
            students[num2]['tel'] = new_tel
            new_qq = input('请输入新QQ:')
            students[num2]['qq'] = new_qq
            print('修改后的名片册为{}'.format(students))
        elif x == '4':
            num3 = input('请输入要查询的名字:')
            for s in students:
                if s.get('name') == num3:
                    print(s)
                    break
            else:
                print('没有找到此人的信息！')
        elif x == '5':
            print('序号      姓名      电话       QQ')
            for i, everyone in enumerate(students):
                print(i, '    ', everyone['name'], '    ', everyone['tel'], '    ', everyone['qq'])
        elif x == '6':
            num4 = input('确定退出系统吗？（yes or no）')
            if num4 == 'yes':
                break
            elif num4 == 'no':
                continue
    else:
        print('请输入正确的操作数字')
