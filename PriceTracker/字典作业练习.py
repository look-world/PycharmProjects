students = [
    {'name': '哈哈', 'age': 21, 'score': 95, 'tel': '13822226287', 'gender': 'unknown'},
    {'name': '张三', 'age': 18, 'score': 98, 'tel': '13888889888', 'gender': 'female'},
    {'name': '李四', 'age': 28, 'score': 99, 'tel': '13833333338', 'gender': 'male'},
    {'name': '王五', 'age': 21, 'score': 98, 'tel': '13822222287', 'gender': 'unknown'},
    {'name': 'jack', 'age': 17, 'score': 58, 'tel': '13882323888', 'gender': 'male'},
    {'name': 'tony', 'age': 23, 'score': 52, 'tel': '138881188998', 'gender': 'female'},
    {'name': 'chris', 'age': 15, 'score': 89, 'tel': '13888115888', 'gender': 'unknown'}
]

# 1.统计不及格的人数
# 2.打印不及格的名字和成绩
# 3.统计未成年的个数
# 4.打印出手机尾号为8的人
count = 0
teenager_conut = 0
for student in students:
    if student['score'] < 60:
        count += 1
        print('%s不及格，分数为%d' % (student['name'], student['score']))
    if student['age'] < 18:
        teenager_conut += 1
    if student['tel'].endswith('8'):
        print('手机尾号为8的是%s' % (student['name']))
print('不及格的人数为%d' % (count))
print('未成年的人数为%d' % (teenager_conut))

# 5.打印最高分和对应的名字
max_score = students[0]['score']  # 假设最高分
# max_index = 0                       #假设最高分元素下标的字典
for i, maxstudent in enumerate(students):  # 用enumerate方法显示for循环的下标i
    if max_score < maxstudent['score']:
        max_score = maxstudent['score']
        # max_index = i
# print(students[max_index]['name'], max_score)
print('最高成绩是%d' % (max_score))
for student in students:  # 因为有相同高分需要再遍历一次打印出相同高分
    if max_score == student['score']:
        print(student['name'])

# 6.删除性别不明的学生

for i, student in enumerate(students):       # 用enumerate方法显示for循环的下标i
    if student['gender'] == 'unknown':
        del students[i]

print(students)

# 7.按学生成绩排序
for j in range(0, len(students) - 1):
    for i in range(0, len(students) - 1):
        if students[i]['score'] < students[i + 1]['score']:
            students[i]['score'], students[i + 1]['score'] = students[i + 1]['score'], students[i]['score']

print(students)
