# 函数的三要素：函数名，参数和返回值
# 在有一些编程语言里，允许函数重名，python不允许重名，后一个会覆盖前一个

# def te(a, b):
#     print('hello,a={},b={}'.format(a, b))


def te(x):
    print('he,x={}'.format(x))


# te = 5
te(3)

# python里可以理解函数名是一个变量名

# input = 2
# input('请输入')


