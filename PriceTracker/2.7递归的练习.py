# 使用递归求n！

def get_c(n):
    if n == 0:
        return 1
    return get_c(n - 1) * n


print(get_c(6))


# 使用递归求斐波那契数列的第n个数字   1,1,2,3,5,8,13
def factorial(n):
    if n - 2 == 0 or n - 1 == 0:  # or或者
        return 1

    return factorial(n - 2) + factorial(n - 1)


print(factorial(5))
