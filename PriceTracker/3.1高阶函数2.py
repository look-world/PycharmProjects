# 1.一个函数作为另一个函数的返回值

def tes():
    print('我是tes函数')
    return 'hello'


def demo():
    print('我是demo函数')
    return tes


def bar():
    print('我是bar函数')
    return tes()


# x = tes()
# print(x)

#
# y = demo()
# print(y)
# z = y()
# print(z)

a = bar()
print(a)

# 2.一个函数作为另一个函数的参数  lambda表达式的使用
# sort filter map reduce
# 3.在函数内部再定义一个函数
