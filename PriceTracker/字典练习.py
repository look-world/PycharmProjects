persons = [
    {'name': 'zhangsan', 'age': 18},
    {'name': 'lisi', 'age': 20},
    {'name': 'wangwu', 'age': 19},
    {'name': 'jerry', 'age': 21}
]

x = input('请输入姓名：')

for names in persons:
    if x == names['name']:
        print('已存在')
        break
else:                                 #注意这里要用break和for else语句
    y = int(input('请输入年龄：'))
    new_persons = {'name': x}         #创建新的字典
    new_persons['age'] = y            #添加age

    persons.append(new_persons)       #将新字典添加到persons列表

    print(persons)
