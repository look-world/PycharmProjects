import copy

words = ['hehe', 'hello', 'yes', [10, 20, 30], 'hi', 'good', 'ok']

words1 = words.copy()  #浅拷贝
words2 = copy.deepcopy(words)  #深拷贝

words[3][0] = 1    #只拷贝外层列表，里面的列表是赋值，所以里面的数组可以更改
print(words1)
print(words2)