# filter 可对可迭代对象过滤，得到的时一个filter对象
# python2时是内置函数，python3修改成了内置类

ages = [23, 12, 15, 17, 25]
# filter第一个参数function是函数，第二个参数iterable是可迭代对象
# filter的结果是一个filter类型对象，filter对象也是一个可迭代对象
x = filter(lambda ele: ele > 18, ages)
# for i in x:
#     print(i)
adult = list(x)
print(adult)
