def add(a: int, b: int):
    '''
    这个函数用来将两个数字相加
    :param a: 第一个数字
    :param b: 第二个数字
    :return: 两个数字的和
    '''
    return a + b


print(add(1, 2))
help(add)

x = add('hello', 'world')  # 只能建议传入的数据类型，但是无法限定
print(x)

# y = add(5 + 'hello')
# print(y)
