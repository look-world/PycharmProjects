# 用三个元组表示三门学科的选课学生姓名（一个学生可以选多门课）
# 1.求选课学生总共有多少人
# 2.求只选了第一个学科的人的数量和对应名字
# 3.求只选了一门学科学生的数量和姓名
# 4.求选了两门学科学生的数量和姓名
# 5.求选了三门学科学生的数量和姓名

sing = ('张三', '李四', '李白', '白居易', '李清照', '杜甫', '王维', '孟浩然', '王安石')
dance = ('张三', '李白', '李清照', '杜甫', '王安石', '哈哈')
rap = ('张三', '李白', '白居易', '李清照', '王维', '苏轼', '王安石')

# 1.求选课学生总共有多少人
dc = sing + dance + rap
total = set(dc)  # set方法去重
print(len(total))

# 2.求只选了第一个学科的人的数量和对应名字
p_list = []
for p in sing:
    if p not in dance and p not in rap:
        p_list.append(p)
print(len(p_list))
print(p_list)

# 3.求只选了一门学科学生的数量和姓名
# 4.求选了两门学科学生的数量和姓名
# 5.求选了三门学科学生的数量和姓名

p_dict = {}
for name in dc:
    if name not in p_dict:
        p_dict[name] = dc.count(name)
print(p_dict)

one = []
tow = []
sera = []
for k, v in p_dict.items():  # 字典拆包遍历
    if v == 1:
        one.append(k)
    elif v == 2:
        tow.append(k)
    elif v == 3:
        sera.append(k)
print('选一门', one)
print('选两门', tow)
print('选三门', sera)
