# a = 13
# b = 20
# c = 0
#
# c = a
# a = b
# b = c
#
# print(a)
# print(b)

nums = [6, 5, 3, 1, 8, 7, 2, 4]
n = 0
count = 0
while n < len(nums) - 1:
    flag = True
    i = 0
    while i < len(nums) - 1 - n:
        count += 1
        if nums[i] > nums[i + 1]:
            flag = False
            nums[i], nums[i + 1] = nums[i + 1], nums[i]
        i += 1
    if flag:
        break
    n += 1
print(nums)
print('一共%d' % (count))
