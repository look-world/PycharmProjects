import time


def cal_time(fn):
    print('cal_time被调用')
    print('fn的值是{}'.format(fn))

    def inner(n, *args, **kwargs):  # 这样写可以传多个参数
        start = time.time()
        s = fn(n)
        end = time.time()
        print('代码耗时{}秒'.format(end - start))
        return s

    return inner


@cal_time  # 第一件事调用函数cal_time；第二件事把被装饰的函数传递给fn
def demo(n):
    x = 0
    for i in range(1, n):
        x += i
    return x


# @cal_time
# def demo2():
#
#     print('hello')
#     time.sleep(3)
#     print('world')


m = demo(100000000, 'hello', x='good')
print(m)

# demo2()
