person = {'name': 'zhangsan', 'age': 18, 'height': '180cm'}

for x in person:  # 遍历循环
    print(x, ':', person[x])

for k in person.keys():
    print(k, '=', person[k])

for v in person.values():  # 只能拿到值
    print(v)

print(person.items())

for item in person.items():
    print(item[0], '=', item[1])

for k, v in person.items():  # 用拆包的方法遍历
    print(k, '=', v)
