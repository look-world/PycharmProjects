def te(a):
    print('修改前a的内存地址0*%X' % id(a))
    a = 100
    print('修改后a的内存地址0*%X' % id(a))


def demo(nums):
    nums[0] = 10


x = 1
print('x调用前的内存地址0*%X' % id(x))
te(x)
print('x调用后的内存地址0*%X' % id(x))
print(x)

y = [2, 3, 5, 3, 5]
demo(y)
print(y)
