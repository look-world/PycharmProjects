a = 100  # 全局变量，在整个py文件中都可以访问
word = '你好'


def text():
    x = 'hello'  # 这个变量是在函数内部定义的变量，它是局部变量，只能在函数内部使用
    print('x={}'.format(x))

    a = 10  # 在函数内定义了一个新的局部变量
    print('函数内部a={}'.format(a))

    # 函数内部使用global修改全局变量
    global word
    word = 'ok'

    print('locals={},globals={}'.format(locals(), globals()))  # 内置函数locals()查看局部变量  globals()查看全局变量


text()
print('函数外部a={}'.format(a))
print('函数外部word={}'.format(word))

# 在python里，只有函数能分割作用域
if 3>2:
    m = 'he'

print(m)
print(globals())
