first = {'张三', '李四', '李白', '白居易', '李清照', '杜甫', '王维', '孟浩然', '王安石'}
second = {'张三', '李白', '李清照', '杜甫', '王安石', '哈哈'}

print(first - second)  # 求差集
print(second - first)
print(first & second)  # 求交集
print(first | second)  # 求并集
print(first ^ second)  # 求差集的并集   删除相同的

