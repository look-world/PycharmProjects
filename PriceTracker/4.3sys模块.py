# sys模块  系统相关的功能
import sys

print('hello')

# ['D:\\PycharmProjects\\PriceTracker',
#  'D:\\PycharmProjects',
#  'D:\\PyCharm 2020.1.1\\plugins\\python\\helpers\\pycharm_display',
#  'D:\\anaconda3\\python37.zip',
#  'D:\\anaconda3\\DLLs',
#  'D:\\anaconda3\\lib',
#  'D:\\anaconda3',
#  'D:\\anaconda3\\lib\\site-packages',
#  'D:\\anaconda3\\lib\\site-packages\\win32',
#  'D:\\anaconda3\\lib\\site-packages\\win32\\lib',
#  'D:\\anaconda3\\lib\\site-packages\\Pythonwin',
#  'D:\\PyCharm 2020.1.1\\plugins\\python\\helpers\\pycharm_matplotlib_backend']
print(sys.path)  # 结果是一个列表，表示查找模块的路径

sys.stdin  # 接收用户的收入

sys.exit(100)  # 退出程序，和内置函数exit()一致
# print('world')
