# 递归就是函数自己调用自己
# 递归最重要的就是要找到出口（停止的条件）


count = 0


def tell_story():
    global count
    count += 1
    print('从现有座山')
    print('山上有座庙')
    print('庙里有个老和尚')
    print('还有一个小和尚')
    # if count > 5:
    #     return
    if count < 5:
        tell_story()


tell_story()
print('_____________________________')
# 求1～n的和
add = 0
count2 = 0


def get_sum(n):
    # global add, count2     #自己瞎写
    # count2 += 1
    # add += count2
    # if count2 < n:
    #     get_sum(n)
    # return add
    if n == 0:
        return 0
    return get_sum(n - 1) + n


print(get_sum(100))
