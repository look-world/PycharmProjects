# 1.一个函数作为另一个函数的返回值
# 2.一个函数作为另一个函数的参数
# 3.在函数内部再定义一个函数

def foo():
    print('我是foo我被调用了')
    return 'foo'

def bar():
    print('我是bar我被调用了')
    return foo

# x = bar()
# print('x={}'.format(x))
#
# x()

y = bar()()
print(y)

def outer():
    n = 90
    def inner():
        m = 100
        print('我是inner函数')

    print('我是outer函数')
    return inner


outer()()


