dict1 = {"a": 100, "b": 200, "c": 300}
x = {}
for k, v in dict1.items():
    # y = {v: k}
    # x.update(y)
    x[v] = k

print(x)

dict2 = {v:k for k,v in dict1.items()}    #字典推导式
print(dict2)


