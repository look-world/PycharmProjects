# def  函数名(参数):
#   函数要执行的操作

def tell_story(count):
    for i in range(count):
        print('从前有座山')
        print('山上有座庙')
        print('庙里有个老和尚')
        print('还有一个小和尚')


age = int(input('请输入孩子的年纪：'))
if 0 <= age < 3:
    tell_story(5)
elif 3 <= age < 5:
    tell_story(1)
