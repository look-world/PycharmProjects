nums1 = [1, 2, 3, 4, 5]
nums2 = [6, 7, 8, 9]
nums1.extend(nums2)  # 合并两个列表
print(nums1)

person1 = {'name': 'zhangsan', 'age': '18'}
person2 = {'addr': '大同', 'height': '180'}
person1.update(person2)  # 合并两个字典
print(person1)

words1 = ('hello', 'good')
words2 = ('yes', 'no')
print(words1 + words2)
