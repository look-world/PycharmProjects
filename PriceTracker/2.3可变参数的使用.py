def add(a, b):
    return a + b


def add_many(m):
    x = 0
    for i in m:
        x += i

    return x


print(add_many((1, 2, 1, 1, 1)))
print(add_many([1, 2, 3, 4, 1, 1, 1]))
print('__________________________________________')


def add2(a, b, *args, mul=1, **kwargs):  # *args表示可位置变参数    mul缺省参数需要写在*args后面  **kwargs表示可变的关键字参数
    # print('a={},b={}'.format(a, b))
    # print('args={}'.format(args))  # 多出来的可变参数会以元组的形式保存在ages里
    print('kwargs={}'.format(kwargs))  # 多出来的关键字参数会以字典的形式保存起来
    c = a + b
    for age in args:
        c += age

    return c * mul


print(add2(1, 2, 3, mul=2, x=0, y=4))
# add2(8, 5, 3, 4, 9)
# add2(8, 6, 5, 6, 4, 2, 8, 9, 4)
