# 有几个内置函数和内置类，用到了匿名函数
nums = [4, 6, 5, 3, 1, 2]
# nums.sort()   #列表的sort方法，会直接对列表进行排序
# print(nums)


ints = (3, 4, 7, 2, 1, 5, 9, 6)
# sorted内置函数，不会改变原有数据，而是生成一个新的有序的列表
x = sorted(ints)
print(ints)

students = [
    {'name': 'zhangsan', 'age': 18, 'score': 98, 'height': 180},
    {'name': 'lisi', 'age': 20, 'score': 97, 'height': 175},
    {'name': 'jac   k', 'age': 25, 'score': 90, 'height': 182},
    {'name': 'tony', 'age': 30, 'score': 100, 'height': 178},
    {'name': 'henry', 'age': 22, 'score': 85, 'height': 185}

]

# 字典和字典之间不能使用比较计算

# 需要传递参数key指定比较规则
# key需要一个函数

# foo() takes 0 positional arguments but 1 was given
# foo()这个函数需要0个位置参数，但是在调用时候给了1个参数

# def foo(ele):
#     # print('ele={}'.format(ele))
#     return ele['age']    #通过返回值告诉sort方法，按照元素的哪个属性进行排序
# 在sort内部实现的时候，调用了foo（）方法，并且传入了一个参数,参数就是列表里的元素
# students.sort(key=foo)

students.sort(key=lambda ele: ele['score'])  # 用lambda匿名函数传递参数
print(students)
