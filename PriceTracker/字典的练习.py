chars = ['a', 'c', 'd', 'x', 'p', 'a', 'f', 'p', 's', 'a']

char_count = {}
for x in chars:
    # if x in char_count:
    #     char_count[x] += 1
    # else:
    #     char_count[x] = 1
    if x not in char_count:
        char_count[x] = chars.count(x)

# print(char_count)
max_count = max(char_count.values())  # 获取出现次数最大值

for k, v in char_count.items():  # 拆包遍历char_count字典，
    if v == max_count:  # 判断值是不是等于最大，如果等于输出最大值的key
        print(k)
