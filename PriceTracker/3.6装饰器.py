import time


def cal_time(fn):
    print('cal_time被调用')
    print('fn的值是{}'.format(fn))

    def inner():
        start = time.time()
        fn()
        end = time.time()
        print('代码耗时{}秒'.format(end - start))

    return inner


@cal_time  # 第一件事调用函数cal_time；第二件事把被装饰的函数传递给fn
def demo():
    x = 0
    for i in range(1, 100000000):
        x += i
    print(x)


# 第三件事：当再次调用demo函数时，装饰的demo函数已经不是上面的demo
print('装饰后的demo={}'.format(demo))
demo()
