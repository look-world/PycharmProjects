nums = [i for i in range(10)]
print(nums)

x = [i for i in range(10) if i % 2 == 0]  # 取偶数
print(x)

y = [i for i in range(10) if i % 2]  # 取偶数，因为if会将余数转换成布尔值，余数为1则为Ture 0为False  所以会输出奇数
print(y)

points = [(i, j) for i in range(5, 9) for j in range(10, 20)]
print(points)

m = [i for i in range(1, 101)]
print(m)

n = [m[j:j + 3] for j in range(0, 100, 3)]
print(n)
