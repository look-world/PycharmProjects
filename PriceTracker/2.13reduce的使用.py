from functools import reduce


# reduce以前是一个内置函数
# 内置函数和内置类都在builtin.py文件里

def foo(x, y):  # x = 100  y = 85;  x = 185  y = 60  ;x = 245  y = 95  ;x = 340 y = 92;432
    return x + y


scores = [100, 85, 60, 95, 92]
print(reduce(foo, scores))

students = [
    {'name': 'zhangsan', 'age': 18, 'score': 98, 'height': 180},
    {'name': 'lisi', 'age': 20, 'score': 97, 'height': 175},
    {'name': 'jac   k', 'age': 25, 'score': 90, 'height': 182},
    {'name': 'tony', 'age': 30, 'score': 100, 'height': 178},
    {'name': 'henry', 'age': 22, 'score': 85, 'height': 185}

]

print(reduce(lambda x, y: x + y['age'], students, 0))     #0是给了x一个初始值0 ；  x = 0  y =  {'name': 'zhangsan', 'age': 18, 'score': 98, 'height': 180}
