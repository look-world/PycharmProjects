# 内置类 list   tuple  set
nums = [9, 8, 4, 3, 2, 1]
x = tuple(nums)  # tuple内置类将list列表转换成元组
print(x)
y = set(x)  # set内置类将元组转换为集合
print(y)

z = list({'name': 'zhangsan', 'age': 18, 'score': 99})
print(z)
# 内置函数eval, 可以执行字符串里的代码
a = 'input(\'请输入你的姓名:\')'
# print(eval(a))

import json
# JSON的使用，吧列表，元组，字典转换成JSON字符串
person = {'name': 'zhangsan', 'age': 18, 'gender': 'female'}
#字典如果想要把它传给前端页面或者把字典写入到一个文件里
m = json.dumps(person)     #dumps方法将字典、列表、集合、元组的转换成JSON字符串
print(m)
print(type(m))  #<class 'str'>s


n = '{"name": "zhangsan", "age": 18, "gender": "female"}'
# p = eval(n)     #将json字符串穿换成字典
# print(p)
# print(type(p))
s = json.loads(n)      #将json字符串转换成python数据类型
print(s)
print(type(s))
