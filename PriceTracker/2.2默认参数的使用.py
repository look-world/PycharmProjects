def say_hello(name, age, city=''):  # 形参city设置一个默认值
    print("大家好，我是{}，我今年{}岁了，我来自{}".format(name, age, city))


say_hello('zhangsan', age=18)  # 位置参数在前，关键字参数在后
say_hello('jack', 20, 'beijing')

# 缺省参数：
# 有些函数的参数是，如果你传递了参数，就使用传递的参数
# 如果没有传递参数就使用默认参数

# print里面的end就是缺省参数
print('hello', '你好', sep='____')
print('hi')
