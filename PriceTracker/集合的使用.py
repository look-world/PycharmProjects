# {}集合和字典都是用大括号
# ｛｝键值对就是字典，单个的值就是集合

person = {'name': 'zhangsan', 'age': 18}  # class 'dict'
x = {'hello', '1', 'good'}  # class 'set'

names = {'zhangsan', 'lisi', 'jack', 'tony', 'jack', 'lisi'}
print(names)         #会输出随机去重的集合
names.add('hehe')
print(names)
# names.clear()
# print(names)    #｛｝表示空字典，set()表示空集合

names.pop()
print(names)    #随机删除一个

names.remove('lisi')     #删除一个指定的元素  删除没有回报错
print(names)

#union 将多个集合合并生成新的集合
#A.updata（B）将B拼到A里
names.update({'李林','粑粑'})
print(names)