persin = {'name': 'zhangsan', 'age': 14, 'addr': '大同'}
# 获取字典的值
print(persin['name'])  # 适用key获取数据  找不到数据会报错
print(persin.get('ag'))  # 找不到数据会返回None
print(persin.get('he', 'meiyou'))  # 可以改变返回的默认值
# 增加字典的值
persin['name'] = 'lisi'  # 存在就改值
persin['gender'] = 'female'  # 不存在会添加一组键值对

# 删除字典的值
x = persin.pop('name')
y = persin.popitem()
del persin['addr']

persin.clear()  # 清空字典
print(persin)
print(x)
print(y)
