x = 'abcdefghijklmnoplqrst'

print(x[6])  # 查找下标为6的元素
print(x[2:9])  # 取下标区间为2-9的元素
print(x[2:])  # 取下标2之后的所有元素
print(x[:9])  # 取下标9之前的所有元素
print(x[2:9:2])  # 2-9步长为2取元素
print(x[15:3:-1])  # -1表示从右向左
print(x[::])  # 打印全部内容
print(x[::-1])  # 倒序
print(x[-9:-5])  # 负数表示从右开始数，最右边的下标是-1
print('-------------------------------')

print(len(x))  # 获取对象长度
print(x.find('l', 4, 20))  # 查找l的下标,后面两个参数为查找的下标区间
print(x.rfind('l'))  # 从右向左查找
# print(x.index('z'))   #查找，如果没有会报错
# print(x.rindex('z'))    #从右向左查找
print('-------------------------------')
world = 'look'
op = world.replace('o', 'l')  # 替换方法replace,把字符串'o'替换成'l'，所有的操作都不会改变原字符串world
print(op)

print('-------------------------------')
y = 'a-b-c-d-f-e-s-g'

print(y.split('-'))  # 将一个字符串切割成一个列表
print(y.rsplit('-', 2))  # 从左向右分割

p = 'abcifngjEsfEv'
print(p.partition('E'))  # 选择E将对象分割成一个三部分的元组
print(p.rpartition('E'))  # 选择E将对象分割成一个三部分的元组,从右面开始

print('-------------------------------')
l = '==  apple     '
print(l.lstrip('='))  # 删除左边空格  参数可以自定义删除内容
print(l.rstrip())  # 删除右边空格
print(l.strip())  # 删除两边空格

print('-------------------------------')
# 将列表转换成字符串

fruits = ['apple', 'orange', 'banana', 'pear']

print('-'.join(fruits))
print('%'.join('hello'))
