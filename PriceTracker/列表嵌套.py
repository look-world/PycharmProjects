import random

teachers = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
rooms = [[], [], []]

for teacher in teachers:
    room = random.choice(rooms)
    room.append(teacher)

print(rooms)


for i, room in enumerate(rooms):       #i为元素下标，enumerate为添加下标
    print('房间号%d里面一共有%d个老师分别是%s'%(i,len(room),room),end=' ')
    for teacher in room:
        print(teacher, end=' ')
    print()
