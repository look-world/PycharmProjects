# 模块：在python里一个py文件，就可以理解为模块
# 不是所有的py文件都能作为一个模块来导入
# 如果想要让一个py文件能够被导入，模块名字必须遵守命名规则
# 内置模块

import time  # 1. import 模块名直接导入一个模块  调用需要time.time()写模块名和函数名
from random import randint  # 2. from 模块名 import 函数名 ，导入一个模块里的方法或变量；randint(0, 3)可以直接使用这个函数
from math import *  # 3. *导入这个模块里的所有方法和变量；可以直接使用所有方法
import datetime as dt  # 4.导入模块并起一个别名
from copy import deepcopy as dp  # 5.from 模块名 import 函数名 as 别名

# 导入后可以用这个模块的变量和方法
print(time.time())
# time.sleep(3)

randint(0, 3)  # 生成随机区间内的随机整数

print(pi)
print(dt.MAXYEAR)

dp(['hello', 'hi'])
