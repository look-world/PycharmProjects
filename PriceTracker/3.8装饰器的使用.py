# 改需求 产品经理
# 如果超过22点不让玩家玩游戏，如果获取不到时间默认无法玩
# 开放封闭原则


def can_play(fn):
    # *args会以元组形式保存后面的参数，调用需要用args[0]来获取传过来的值；**kwargs是以字典形式保存后面的参数，调用参数kwargs['clock']获取
    def play_clock(name, game, *args, **kwargs):
        clock = kwargs.get('clock', 20)
        if clock > 22:
            print('太晚了，不能玩了')
        else:
            fn(name, game)

    return play_clock


@can_play
def play_game(name, game):
    print('{}正在玩{}'.format(name, game))


play_game('张三', 'dnf', x='hello', y='good', clock=18)
play_game('李四', '命运2')
