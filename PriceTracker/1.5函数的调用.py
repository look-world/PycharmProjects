def te1():
    print('test1开始了')
    print('test1结束啦')


def te2():
    print('test2开始了')
    te1()
    print('test2结束啦')


te2()


# 定义一个函数求n~m所有整数之和

def add(n, m):
    x = 0
    for i in range(n, m + 1):
        x += i
    return x


print(add(1, 100))


# 求一个n的阶乘
def fac(n):
    x = 1
    for i in range(1, n + 1):
        x *= i
    return x


print(fac(3))


# 计算m阶乘的和 m=6 ==> 1!+2!+3!+4!+5!+6!
def factorial(m):
    x = 0
    for i in range(1, m + 1):
        x += fac(i)
    return x


print(factorial(5))
