from tkinter import *


def decrypte():
    encryption = input("输入解密文字：")
    encryption_shift = int(input("你好，请输入你想回移的个数"))
    another_cipherText = ""
    for c in encryption:
        if c in encryption:
            if c.isalpha():
                another_stayInAlphabet = ord(c) - encryption_shift
            if another_stayInAlphabet > ord("z"):
                another_stayInAlphabet += 26
            another_finalLetter = chr(another_stayInAlphabet)
            another_cipherText += another_finalLetter
    print("解密完成：", another_cipherText, "通过shift", encryption_shift, "位解密")


def encrypt():
    plainText = input("输入加密文字：")
    shift = int(input("输入平移个数："))
    cipherText = ""

    for ch in plainText:
        if ch.isalpha():
            stayInAlphabet = ord(ch) + shift
        if stayInAlphabet > ord("z"):
            stayInAlphabet -= 26
        finalLetter = chr(stayInAlphabet)
        cipherText += finalLetter
    print("加密完成", cipherText, "通过shift", shift, "位加密")


menu = Tk()
menu.title("加解密小程序")
menu.geometry("300x200")
button1 = Button(menu, text="加密", command=encrypt)
button1.pack()

button2 = Button(menu, text="解密", command=decrypte)
button2.pack()

button3 = Button(menu, text="退出", command=exit)
button3.pack()

menu.mainloop()
