import win32com.client as win32
import pythoncom


class ApplicationEvents:
    def OnSheetActivate(self, *args):
        print("选中的这个Sheet")


class WorkbookEvents:
    def OnSheetSelectionChange(self, *args):
        print(args[1].Address)


excel1 = win32.GetActiveObject("Excel.Application")
excel1_events = win32.WithEvents(excel1, ApplicationEvents)

excel1_workbook = excel1.Workbooks("工作簿1")
excel1_workbook_events = win32.WithEvents(excel1_workbook, WorkbookEvents)

while True:
    pythoncom.PumpWaitingMessages()
