import xlwings as  xw
import time

# 写入数据
# app = xw.App(visible=True,add_book=False)    #打开空工作簿，visible=True是显示工作簿，add_book=False是不新建工作表
# workbook = app.books.add()    #新建工作表
# workbook.sheets["sheet1"].range("A1").value="编号"     #选中sheet1的A1输入编号
# workbook.sheets["sheet1"].range("B1").value="姓名"
# workbook.sheets["sheet1"].range("C1").value="工资收入"

# 读取数据
# app = xw.App(visible=True,add_book=False)
#
# path=r"C:\Users\Smile\Desktop\活动奖励.xlsx"
# wb = app.books.open(path)    #打开链接表格
# cell = xw.books["活动奖励.xlsx"].sheets["sheet1"].range("B2").value   #读取数据
# print(cell)

list_header = ["编号", "姓名", "收入水平"]
list_content = [1, "小米", "8000"]

app = xw.App(visible=True, add_book=False)
wb = app.books.add()
wb.sheets["sheet1"].range("A1:C1").value = list_header
wb.sheets["sheet1"].range("A2:C2").value = list_content
now = time.strftime("%Y-%m-%d", time.localtime())
wb.save(r"C:\Users\Smile\Desktop\股东" + now + ".xlsx")

time.sleep(3)
wb.close()
app.quit()
