import xlwings as  xw
import time

app = xw.App(visible=True, add_book=False)

path = r"C:\Users\Smile\Desktop\股东核对.xlsx"  # 表格链接
wb1 = app.books.open(path)  # 打开链接表格
a1 = wb1.sheets["sheet1"].range("B2786:E2825").value  # 复制

wb2 = app.books.add()
wb2.sheets["sheet1"].range("A1").value = a1  # 粘贴
wb2.sheets["sheet1"].range("B:B").api.Delete()  # 删除B列
wb2.sheets["sheet1"].range("A1:E200").columns.autofit()  # 美化表格

now = time.strftime("%Y-%m-%d", time.localtime())  # 获取当前时间，并转换格式
wb2.save(r"C:\Users\Smile\Desktop\股东", now, ".xlsx")  # 另存文件，文件名为时间

wb2.close()
app.quit()
