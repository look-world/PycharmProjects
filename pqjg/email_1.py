import smtplib
import os
from email.mime.text import MIMEText


def send_email():
    host = 'smtp.126.com'  # SMTP端口
    port = 25  # 端口号固定25
    sender = os.environ.get('EMAIL_USER')  # 环境变量加密邮箱账户
    sender_alias = 'll<ll_look_world@126.com>'
    password = os.environ.get('EMAIL_PASS')  # 环境变量加密邮箱密码
    receiver = 'll_look_world@126.com'  # 发送邮箱
    body = '<h1>这是一封测试邮件</h1>'  # 邮件内容
    msg = MIMEText(body, 'html')  # 内容转码
    msg['subject'] = '是ll的测试邮件哦'  # 邮件标题
    msg['from'] = sender_alias  # 发送者
    msg['to'] = receiver  # 发送给谁

    s = smtplib.SMTP(host, port)
    s.login(sender, password)
    s.sendmail(sender, receiver, msg.as_string())


if __name__ == '__main__':
    send_email()
