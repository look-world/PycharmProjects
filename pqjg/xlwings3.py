import random
import xlwings as xw
import time


def random_int_list(start, stop, length):
    start, stop = (int(start), int(stop)) if start <= stop else (int(stop), int(start))
    length = int(abs(length)) if length else 0
    random_list = []
    for i in range(length):
        random_list.append(random.randint(start, stop))
    return random_list


agent_code = random_int_list(1, 10000, 10000)  # 生成从1开始10000结束随机的10000组数据，模拟爬取数据
agent_work = random_int_list(1, 999999, 10000)  # 生成随机的数据，模拟数据

app = xw.App(visible=True, add_book=False)

wb = app.books.add()
wb.sheets["sheet1"].range("A1").value = "特工编号"
wb.sheets["sheet1"].range("B1").value = "任务编号"

wb.sheets["sheet1"].range("A2").options(transpose=True).value = agent_code  # options(transpose=True)为转置数据横转竖
wb.sheets["sheet1"].range("B2").options(transpose=True).value = agent_work  # options(transpose=True)为转置数据横转竖

time.sleep(5)
wb.close()
app.quit()
