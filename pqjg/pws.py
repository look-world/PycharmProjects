def encrypt():
    plainText = input("输入加密文字：")
    shift = int(input("输入平移个数："))
    cipherText = ""

    for ch in plainText:
        if ch.isalpha():
            stayInAlphabet = ord(ch) + shift
        if stayInAlphabet > ord("z"):
            stayInAlphabet -= 26
        finalLetter = chr(stayInAlphabet)
        cipherText += finalLetter
    print("加密完成", cipherText, "通过shift", shift, "位加密")


encrypt()
